//Business logic

function placesVisited(name, location, landmarks, timeYear, notes) {
  this.name = name;
  this.location = location;
  this.landmarks = landmarks;
  this.timeYear = timeYear;
  this.notes = notes;
}

placesVisited.prototype.nameLocation = function() {
  return this.name + " " + this.location;
}

//User interface logic
$(document).ready(function() {
  $("form#new-place").submit(function(event) {
    event.preventDefault();

    var inputtedName = $("input#nameId").val();
    var inputtedLocation = $("input#locationId").val();
    var inputtedLandmarks = $("input#landmarksId").val();
    var inputtedTimeYear = $("input#timeYearId").val();
    var inputtedNotes = $("input#notesId").val();

    var newPlace = new placesVisited(inputtedName, inputtedLocation, inputtedLandmarks, inputtedTimeYear, inputtedNotes);

    $("ul#places").append("<li><span class='place'>" + newPlace.nameLocation() + "</span></li>");
    $(".place").last().click(function() {
      $("#show-place").show();
      $("#show-place h2").text(newPlace.nameLocation());
      $(".name").text(newPlace.name);
      $(".location").text(newPlace.location);
      $(".landmarks").text(newPlace.landmarks);
      $(".timeYear").text(newPlace.timeYear);
      $(".notes").text(newPlace.notes);
    });

    $("input#nameId").val("");
    $("input#locationId").val("");
    $("input#landmarksId").val("");
    $("input#timeYearId").val("");
    $("input#notesId").val("");
  });
});
